import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { RestProvider } from "../../providers/rest/rest";

/**
 * Generated class for the ContactoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-contacto",
  templateUrl: "contacto.html"
})
export class ContactoPage {
  public _contactosData: any;
  public contactosData = [];
  private loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });
    this.loading.present();
    this.getContactosData();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ContactoPage");
  }

  getContactosData() {
    this.restProvider
      .getContactosData()
      .then(data => {
        let contactData: any = data;
        Object.values(contactData).forEach(element => {
          let elementData: any = element;
          let direction = elementData.direccion.split(" ");
          elementData.directionCode = direction.join("+");
          this.contactosData.push(elementData);
        });
        this.loading.dismiss();
      })
      .catch(err => {
        console.log(err);
      });
  }
}
